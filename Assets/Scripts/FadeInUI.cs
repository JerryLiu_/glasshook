using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInUI : MonoBehaviour
{
    [SerializeField] float delayTime = 0.5f;
    private CanvasGroup canvasGroup;

    void OnEnable()
    {
        canvasGroup = this.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0.0f;

        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        float time = 0.0f;
        float duration = 1.0f;

        yield return new WaitForSeconds(delayTime);

        while (canvasGroup.alpha < 1.0f)
        {
            canvasGroup.alpha = Mathf.Lerp(0, 1, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
    }
}
