using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScore : MonoBehaviour
{
    [SerializeField] private Score scoreObject;
    [SerializeField] private Text highScoreText;
    [SerializeField] private GameObject newHighScoreText;
    private Text score;

    void OnEnable()
    {
        score = this.GetComponent<Text>();

        score.text = "Score: " + scoreObject.GetScore().ToString() + "m";
        highScoreText.text = "High:" + scoreObject.GetHighScore().ToString() + "m";

        if (scoreObject.CheckIfNewHighScore())
        {
            highScoreText.color = new Color(241, 0, 0);
        }
        else
        {
            highScoreText.color = new Color(.196f, .196f, .196f);
            newHighScoreText.SetActive(false);
        }
    }
}
