﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeController : MonoBehaviour
{
    [SerializeField] float despawnDistance, rotationSpeed;
    [SerializeField] private GameObject dangerCircle;
    private enum Direction {clockwise, counterclockwise};
    private enum HookStatus {notHooked, hooking, hooked};
    Direction rotationDirection;
    HookStatus hookStatus;
    private Transform spikes;
    private Coroutine extend;
    private SpriteRenderer nodeRenderer, spikesRenderer;
    private SpriteMask spriteMask;
    Camera mainCam;
    
    void Start()
    {
        mainCam = Camera.main;
        spikes = this.transform.GetChild(0);

        rotationDirection = Random.value > 0.5f ? Direction.clockwise : Direction.counterclockwise;
        nodeRenderer = this.GetComponent<SpriteRenderer>();
        spikesRenderer = spikes.GetComponent<SpriteRenderer>();
        spriteMask = SpriteMask.FindObjectOfType<SpriteMask>();
        hookStatus = HookStatus.notHooked;
    }

    void Update()
    {
        if (this.transform.position.y < mainCam.transform.parent.position.y - despawnDistance)
        {
            this.gameObject.SetActive(false);
        }

        if (rotationDirection == Direction.clockwise)
        {
            this.gameObject.transform.Rotate(Vector3.back * (rotationSpeed * Time.deltaTime));

            if (dangerCircle.activeSelf)
                dangerCircle.transform.Rotate(Vector3.back * (rotationSpeed * Time.deltaTime));
        }
        else if (rotationDirection == Direction.counterclockwise)
        {
            this.gameObject.transform.Rotate(Vector3.forward * (rotationSpeed * Time.deltaTime));

            if (dangerCircle.activeSelf)
                dangerCircle.transform.Rotate(Vector3.forward * (rotationSpeed * Time.deltaTime));
        }
    }

    public void extendSpikes()
    {
        extend = StartCoroutine(eSpikes());
    }

    public void retractSpikes()
    {
        StopCoroutine(extend);
        StartCoroutine(rSpikes());
    }

    IEnumerator eSpikes()
    {
        float time = 0.0f;
        float duration = 0.15f;
        Vector3 start = spikes.transform.localScale;
        Vector3 target = new Vector3(1, 1, 1);

        Color startColor = nodeRenderer.color;
        Color targetColor = Color.red;
        Color currentColor;

        while (spikes.transform.localScale != target)
        {
            time += Time.deltaTime;

            spikes.transform.localScale = Vector3.Lerp(start, target, time / duration);
            currentColor = Color.Lerp(startColor, targetColor, time / duration);

            nodeRenderer.color = currentColor;
            spikesRenderer.color = currentColor;
            yield return null;
        }
    }

    IEnumerator rSpikes()
    {
        float time = 0.0f;
        float duration = 0.15f;
        Vector3 start = spikes.transform.localScale;
        Vector3 target = new Vector3(0.5f, 0.5f, 0.5f);

        Color startColor = nodeRenderer.color;
        Color targetColor = Color.black;
        Color currentColor;

        while (spikes.transform.localScale != target)
        {
            time += Time.deltaTime;

            spikes.transform.localScale = Vector3.Lerp(start, target, time / duration);
            currentColor = Color.Lerp(startColor, targetColor, time / duration);

            nodeRenderer.color = currentColor;
            spikesRenderer.color = currentColor;
            yield return null;
        }
    }

    public void warningOn()
    {
        dangerCircle.SetActive(true);
    }

    public void warningOff()
    {
        dangerCircle.SetActive(false);
    }

    public void setHooking(Vector3 direction)
    {
        hookStatus = HookStatus.hooking;
        setSpriteMask(direction);
    }

    public void setHooked()
    {
        hookStatus = HookStatus.hooked;
    }

    public void setNotHooked()
    {
        hookStatus = HookStatus.notHooked;
    }

    private void setSpriteMask(Vector3 direction)
    {
        spriteMask.transform.position = this.transform.position;
        spriteMask.transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }
}
