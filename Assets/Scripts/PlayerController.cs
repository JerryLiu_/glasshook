﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerController : MonoBehaviour
{
    private float p_width; //Portrait width, height. Figure out coordinate system later
    private float p_height;
    [SerializeField] private float hookForce, criticalDistance, criticalFactor, /*weakCriticalDistance,*/ minimumCriticalSpeed, hookConeWidth, deadzoneRadius, killDistance, dangerDistance;
    [SerializeField] private GameObject directionMarkerPrefab = null;
    [SerializeField] private GameObject TouchMarker = null;
    [SerializeField] private GameObject TouchMarkerArrow = null;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private ParticleSystem deathExplosion;
    [SerializeField] private AudioSource hookFire, hookMisfire, hookDisconnect, hookCritical;
    private Camera mainCam;
    private Vector2 startPos;
    private Vector2 direction;
    private GameObject hookedNode = null;
    private GameObject directionMarker;
    private LineRenderer tether;
    private Rigidbody2D rb;

    void Awake()
    {
        p_width = (float)Screen.height;
        p_height = (float)Screen.width;
        tether = this.gameObject.GetComponent<LineRenderer>();
        tether.startWidth = .075f;
        tether.endWidth = .075f;
        tether.material = new Material(Shader.Find("Sprites/Default"));
        tether.startColor = Color.black;
        tether.endColor = Color.red;
        rb = this.GetComponent<Rigidbody2D>();
        mainCam = Camera.main;

        //guide = guideObject.AddComponent<LineRenderer>();
        //guide.startWidth = .1f;
        //guide.endWidth = .1f;
    }

    void Start()
    {
        Time.timeScale = 1;
        directionMarker = (GameObject)Instantiate(directionMarkerPrefab, this.transform);
        directionMarker.transform.localPosition = new Vector3(0f, 0f, 0.01f);
        directionMarker.transform.localScale = new Vector3(3.33f, 3.5f, 1.0f);
        directionMarker.SetActive(false);
    }

    void Update()
    {
        Vector3 bottomLeft = mainCam.ScreenToWorldPoint(new Vector3(0, 0, 0));

        if (this.transform.position.y < mainCam.transform.position.y - killDistance && !hookedNode)
        {
            Vector3 explosionPosition = new Vector3(transform.position.x, bottomLeft.y, 0);
            Die(explosionPosition);
        }

        if (hookedNode)
        {
            if (Vector3.Distance(this.transform.position, hookedNode.transform.position) < dangerDistance) 
            {
                hookedNode.GetComponent<NodeController>().warningOn();
            }
            else
            {
                hookedNode.GetComponent<NodeController>().warningOff();
            }

            if (hookedNode.transform.position.y < bottomLeft.y)
            {
                DisengageHook();
            }
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    DisengageHook();
                    startPos = touch.position;
                    SetTouchMarker(startPos);
                    break;
                case TouchPhase.Moved:
                    direction = (touch.position - startPos) * -1;

                    if (Vector3.Distance(touch.position, startPos) > deadzoneRadius)
                    {
                        TouchMarkerArrow.SetActive(true);
                        PullArrow(Vector3.Distance(touch.position, startPos) - deadzoneRadius);
                    }
                    else
                    {
                        TouchMarkerArrow.SetActive(false);
                    }
                    break;
                case TouchPhase.Ended:
                    resetArrow();
                    if (Vector3.Distance(touch.position, startPos) > deadzoneRadius)
                    {
                        FireHook(direction);
                    }
                    break;
            }
            SetArrow(direction);
            SetTouchMarkerArrow(direction);
        }
        else
        {
            directionMarker.SetActive(false);
            TouchMarker.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        if (hookedNode != null)
        {
            EngageHook(hookedNode);
            DrawLine(hookedNode);
        }
    }

    /* Activates direction marker and sets angle */
    private void SetArrow(Vector2 direction)
    {
        float rotationZ = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        directionMarker.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ);
        directionMarker.SetActive(true);
    }

    private void SetTouchMarker(Vector2 position)
    {
        TouchMarker.transform.position = mainCam.ScreenToWorldPoint(new Vector3(position.x, position.y, mainCam.nearClipPlane));
        TouchMarker.transform.localScale = new Vector3(0.3f, 0.3f, 1.0f);
        TouchMarker.SetActive(true);
        TouchMarkerArrow.SetActive(false);
        StartCoroutine(PopInMarker(TouchMarker.transform));
    }

    private void PullArrow(float distance)
    {
        float maxDistance = 400f;
        Vector3 startScale = new Vector3(3.33f, 3.5f, 1.0f);
        Vector3 endScale = new Vector3(3.33f, 2.9f, 1.0f);

        directionMarker.transform.localScale = Vector3.Slerp(startScale, endScale, distance / maxDistance);
    }

    private void resetArrow()
    {
        directionMarker.transform.localScale = new Vector3(3.33f, 3.5f, 1.0f);
    }

    IEnumerator PopInMarker(Transform transform)
    {
        float time = 0.0f;
        float duration = 0.1f;
        Vector3 startScale = new Vector3(0.01f, 0.01f, 1f);
        Vector3 targetScale = transform.localScale;

        while (time < duration)
        {
            time += Time.unscaledDeltaTime;

            transform.localScale = Vector3.Lerp(startScale, targetScale, time / duration);
            yield return null;
        }
        transform.localScale = targetScale;
    }

    private void SetTouchMarkerArrow(Vector2 direction)
    {
        float rotationZ = Vector3.SignedAngle(TouchMarkerArrow.transform.localPosition, new Vector3(direction.x, direction.y, 0), Vector3.forward);

        TouchMarkerArrow.transform.RotateAround(TouchMarker.transform.position, Vector3.forward, rotationZ);
    }

    private void FireHook(Vector3 direction)
    {
        List<RaycastHit2D> hits = Conecast(transform.position, direction);

        hits.RemoveAll(h => !(h.transform != null && h.transform.CompareTag("Node") && h.transform.GetComponent<SpriteRenderer>().isVisible)); //remove raycasts that hit the wall or non-visible nodes

        if (hits.Count > 0)
        {
            GameObject hitNode = hits.OrderByDescending(h => h.distance).First().transform.gameObject;
            hitNode.GetComponent<NodeController>().setHooking(hitNode.transform.position - this.transform.position);
            hookFire.Play();
            StartCoroutine(LaunchArrow(hitNode.transform));
        }
        else
        {
            hookMisfire.Play();
            StartCoroutine(FlashColor(new Color(173 / 255f, 5 / 255f, 5 / 255f, 1)));
        }
    }

    IEnumerator LaunchArrow(Transform node)
    {
        //GameObject arrow = (GameObject)Instantiate(directionMarkerPrefab, this.transform.position, directionMarker.transform.rotation);
        GameObject arrow = (GameObject)Instantiate(directionMarkerPrefab, this.transform.position, Quaternion.LookRotation(Vector3.forward, node.transform.position - this.transform.position));
        SpriteRenderer arrowRenderer = arrow.GetComponent<SpriteRenderer>();
        arrowRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        arrow.SetActive(true);
        float speed = 50;
        float velocity = speed * Time.deltaTime;

        do
        {
            arrow.transform.position = Vector3.MoveTowards(arrow.transform.position, node.position, velocity);
            DrawLine(arrow);
            yield return null;
        } while (arrow.transform.position != node.position && Input.touchCount < 1);

        if (Input.touchCount < 1)
        {
            hookedNode = node.gameObject;
            hookedNode.GetComponent<NodeController>().setHooked();
            hookedNode.transform.GetChild(1).gameObject.layer = 10;
            hookedNode.GetComponent<NodeController>().extendSpikes();
        }

        Destroy(arrow);
    }

    private List<RaycastHit2D> Conecast(Vector3 position, Vector3 centerDirection)
    {
        Vector3 leftSide = Quaternion.AngleAxis(hookConeWidth / 2, Vector3.forward) * centerDirection;
        Vector3 rightSide = Quaternion.AngleAxis(-hookConeWidth / 2, Vector3.forward) * centerDirection;

        Debug.DrawRay(transform.position, leftSide, Color.green, 2f);
        Debug.DrawRay(transform.position, centerDirection, Color.green, 2f);
        Debug.DrawRay(transform.position, rightSide, Color.green, 2f);

        List<RaycastHit2D> hits = new List<RaycastHit2D>
        {   
            Physics2D.Raycast(position, leftSide),
            Physics2D.Raycast(position, centerDirection),
            Physics2D.Raycast(position, rightSide)
        };

        return hits;
    }

    private void EngageHook(GameObject node)
    {
        Vector3 direction = Vector3.Normalize(node.transform.position - this.transform.position);
        rb.AddForce(direction * hookForce);
    }

    private void DisengageHook()
    {
        if (hookedNode)
        {
            NodeController nodecontroller = hookedNode.GetComponent<NodeController>();
            nodecontroller.retractSpikes();
            nodecontroller.warningOff();
            nodecontroller.setNotHooked();
            hookedNode.transform.GetChild(1).gameObject.layer = 9;

            if (Vector3.Distance(this.transform.position, hookedNode.transform.position) < criticalDistance)
            {
                mainCam.GetComponent<WorldController>().DoStrongScreenFreeze();
                hookCritical.Play();
                if ((rb.velocity * criticalFactor).magnitude < minimumCriticalSpeed)
                {
                    rb.velocity = rb.velocity.normalized * minimumCriticalSpeed;
                }
                else
                {
                    rb.velocity *= criticalFactor;
                }
            }
            else
            {
                hookDisconnect.Play();
            }
            /* Removed weak critical mechanic */
            //else if (Vector3.Distance(this.transform.position, hookedNode.transform.position) > weakCriticalDistance)
            //{
            //    rb.velocity *= 0.5f;
            //    StartCoroutine(FlashColor(new Color(173/255f, 5/255f, 5/255f, 1)));
            //}
        }
        hookedNode = null;
        tether.positionCount = 0;
    }

    private void DrawLine(GameObject target)
    {
        tether.positionCount = 2;
        tether.SetPosition(0, this.transform.position);
        tether.SetPosition(1, target.transform.position);
    }

    public void Flash()
    {
        StartCoroutine(FlashColor(Color.white));
    }

    IEnumerator FlashColor(Color color)
    {
        float time = 0.0f;
        float duration = 0.3f;

        Color targetColor = Color.black;
        SpriteRenderer renderer = this.gameObject.GetComponent<SpriteRenderer>();

        renderer.color = color;
        yield return new WaitForSeconds(.2f);

        while (renderer.color != Color.black)
        {
            time += Time.deltaTime;
            renderer.color = Color.Lerp(color, targetColor, time / duration);
            yield return null;
        }
    }

    public void Die()
    {
        if (hookedNode)
            hookedNode.SetActive(false);
        Instantiate(deathExplosion, this.transform.position, Quaternion.AngleAxis(0, Vector3.back));
        deathExplosion.Play();
        gameObject.SetActive(false);
        Score.SaveHighScore();

        TouchMarker.SetActive(false);
        mainCam.GetComponent<ShakeIn2D>().Shake();
        gameOverScreen.SetActive(true);
    }

    private void Die(Vector3 explosionPosition)
    {
        Instantiate(deathExplosion, explosionPosition, Quaternion.AngleAxis(0, Vector3.back));
        deathExplosion.Play();
        gameObject.SetActive(false);
        Score.SaveHighScore();

        TouchMarker.SetActive(false);
        mainCam.GetComponent<ShakeIn2D>().Shake();
        gameOverScreen.SetActive(true);
    }
}