﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Text highScoreText;
    private Text score;
    private int highestPosition;
    private static int highScore;
    private bool isNewHighScore;

    private void Awake()
    {
        score = this.GetComponent<Text>();
        highestPosition = 0;
        isNewHighScore = false;

        score.text = highestPosition.ToString() + "m";
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = "High:" + highScore.ToString() + "m";
    }

    void Update()
    {
        int currentPosition = (int)Mathf.Floor(player.position.y);

        if (currentPosition > highestPosition)
        {
            highestPosition = currentPosition;
            score.text = highestPosition.ToString() + "m";
        }
        if (highestPosition > highScore)
        {
            highScore = highestPosition;
            highScoreText.text = "High:" + highScore.ToString() + "m";
            isNewHighScore = true;
        }
    }

    public int GetScore()
    {
        return highestPosition;
    }

    public int GetHighScore()
    {
        return highScore;
    }

    public bool CheckIfNewHighScore()
    {
        return isNewHighScore;
    }

    public static void SaveHighScore()
    {
        PlayerPrefs.SetInt("HighScore", highScore);
        PlayerPrefs.Save();
    }
}
