﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreenScript : MonoBehaviour
{
    [SerializeField] private string newGameScene = null;
    [SerializeField] private GameObject creditsScreen;

    public void NewGame()
    {
        SceneManager.LoadScene(newGameScene);
    }

    public void ShowCredits()
    {
        creditsScreen.SetActive(true);
    }

    public void HideCredits()
    {
        creditsScreen.SetActive(false);
    }
}
