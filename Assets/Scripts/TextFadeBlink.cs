using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFadeBlink : MonoBehaviour
{
    [SerializeField] float speed;
    Text text;
    Color textColor;

    // Start is called before the first frame update
    void Start()
    {
        text = this.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        textColor.a = (Mathf.Sin(Time.time * speed) + 1.0f) / 2.0f;
        text.color = textColor;
    }
}
