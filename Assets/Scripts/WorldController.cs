﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WorldController : MonoBehaviour
{
    [SerializeField] private GameObject nodePrefab = null;
    [SerializeField] private int numNodes, nodePoolSize;
    [SerializeField] private float lowerBound, levelWidth, nodeSpawnStep, nodeSpawnBuffer, nodeMinSpread, heightTextX, heightTextScale;
    [SerializeField] private Text heightText;
    [SerializeField] private GameObject player, distanceMarkerLine;
    private RectTransform heightTextRect;

    private float highestNodeLevel;

    private List<GameObject> nodePool;

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        initializePool();
        highestNodeLevel = lowerBound;
        heightTextRect = heightText.GetComponent<RectTransform>();
        heightTextRect.anchoredPosition = new Vector2(heightTextX * heightTextScale, 100f * heightTextScale);
        distanceMarkerLine.transform.position = new Vector3(-2, 99.9f, 0);
    }

    private void Update()
    {
        while (this.transform.position.y + nodeSpawnBuffer > highestNodeLevel)
        {
            spawnSection();
        }

        if (this.transform.position.y > heightTextRect.anchoredPosition.y / heightTextScale + 50)
        {
            distanceMarkerLine.transform.position = new Vector3(-2, distanceMarkerLine.transform.position.y + 100, 0);
            heightTextRect.anchoredPosition = new Vector2(heightTextX * heightTextScale, heightTextRect.anchoredPosition.y + 100f * heightTextScale);
            heightText.text = (heightTextRect.anchoredPosition.y / heightTextScale).ToString();
        }
    }

    private void spawnSection()
    {
        for (int i = 0; i < numNodes; i++)
        {
            Vector3 spawnPosition;
            do
            {
                spawnPosition = new Vector3(Random.Range(-levelWidth, levelWidth), Random.Range(highestNodeLevel, highestNodeLevel + nodeSpawnStep), -0.1f);
            } while (isCrowded(spawnPosition));

            GameObject node = getPooledNode();
            node.transform.SetPositionAndRotation(spawnPosition, Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
            node.SetActive(true);
        }

        highestNodeLevel += nodeSpawnStep;
    }

    private bool isCrowded(Vector3 nodePosition)
    {
        int mask = LayerMask.GetMask("Nodes");
        Collider2D node = Physics2D.OverlapCircle(nodePosition, nodeMinSpread, mask);
        if (node != null)
        {
            return true;
        }
        return false;
    }

    private void initializePool()
    {
        nodePool = new List<GameObject>();

        for (int i = 0; i < nodePoolSize; i++)
        {
            GameObject node = (GameObject)Instantiate(nodePrefab);
            node.SetActive(false);
            nodePool.Add(node);
        }
    }

    private GameObject getPooledNode()
    {
        for (int i = 0; i < nodePool.Count; i++)
        {
            if (!nodePool[i].activeInHierarchy)
            {
                return nodePool[i];
            }
        }

        return null;
    }

    public static void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void DoStrongScreenFreeze()
    {
        StartCoroutine(ScreenFreeze(0.15f, true));
    }

    IEnumerator ScreenFreeze(float duration, bool crit)
    {
        float originalTimeScale = Time.timeScale;
        Time.timeScale = 0f;

        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = originalTimeScale;

        if (crit)
        {
            player.GetComponent<PlayerController>().Flash();
        }
    }
}
