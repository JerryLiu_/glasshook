# Grappling
A casual mobile game made with Unity

### [Available on the Play Store](https://play.google.com/store/apps/details?id=com.JerryLiu.Grappling&hl=en_US)

![](/Assets/Gameplay_demo.mp4)

## Gameplay
Tap and drag to bring up the aiming reticle.

Release to shoot your grappling hook in the opposite direction that you dragged.

If you aimed at a node you will latch to it and begin pulling yourself toward it.

Tap again once you have some speed to release the hook and fling yourself upward. If you time your release when you are close to the node you will receive an addition burst of speed.
Don't release too late though; if you touch a node that you are still hooked onto it's game over!
